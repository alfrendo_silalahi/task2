<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="CSS/ganjilgenap.css">

  <title>Ganjil Genap</title>
</head>
<body>
  <div class="container">
  <h1>Ganjil Genap</h1>
  <div>
    <label for="">value</label>
    <input class="value" type="number">
  </div>
  <br>
  <div>
    <button class="generate">Generate</button>
  </div>
  <br>
  <div>
    <span>Hasil : </span>
    <span class="hasil">Masukkan Value</span>
  </div>
  </div>
  <script src="JS/ganjilgenap.js"></script>
</body>
</html>