"use strict";

let value1 = document.querySelector(".value1");
let value2 = document.querySelector(".value2");
const jumlah = document.querySelector(".jumlah");
const kurang = document.querySelector(".kurang");
const kali = document.querySelector(".kali");
const bagi = document.querySelector(".bagi");
let hasil = document.querySelector(".hasil");

let perhitungan;

jumlah.addEventListener("click", function () {
    perhitungan = parseInt(value1.value) + parseInt(value2.value);
    hasil.textContent = perhitungan;
});

kurang.addEventListener("click", function () {
    perhitungan = parseInt(value1.value) - parseInt(value2.value);
    hasil.textContent = perhitungan;
});

kali.addEventListener("click", function () {
    perhitungan = parseInt(value1.value) * parseInt(value2.value);
    hasil.textContent = perhitungan;
});

bagi.addEventListener("click", function () {
    perhitungan = parseInt(value1.value) / parseInt(value2.value);
    hasil.textContent = perhitungan;
});
