"use strict";

const value = document.querySelector(".value");
const generate = document.querySelector(".generate");
const hasil = document.querySelector(".hasil");

generate.addEventListener("click", function () {
    if (parseInt(value.value) % 2 === 0) {
        hasil.textContent = `${value.value} adalah bilangan genap`;
    } else {
        hasil.textContent = `${value.value} adalah bilangan ganjil`;
    }
});
