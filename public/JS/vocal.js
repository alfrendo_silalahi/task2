"use strict";

const input = document.querySelector(".input");
const generate = document.querySelector(".generate");
const hasil = document.querySelector(".hasil");

generate.addEventListener("click", function () {
    hasil.textContent = vocal(input.value);
});

const vocal = (str) => {
    let vocals = /[aeiou]/gi;
    let result = str.match(vocals);

    const set = new Set();
    for (let i = 0; i < result.length; i++) {
        set.add(result[i]);
    }

    const arr = [...set];
    arr.sort();

    let output = `${str} = ${arr.length} -> `;

    for (let newArr of arr) {
        output += `${newArr} `;
    }

    return output;
};
